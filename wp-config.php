<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file definisce le seguenti configurazioni: impostazioni MySQL,
 * Prefisso Tabella, Chiavi Segrete, Lingua di WordPress e ABSPATH.
 * E' possibile trovare ultetriori informazioni visitando la pagina: del
 * Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Editing wp-config.php}. E' possibile ottenere le impostazioni per
 * MySQL dal proprio fornitore di hosting.
 *
 * Questo file viene utilizzato, durante l'installazione, dallo script
 * di creazione di wp-config.php. Non � necessario utilizzarlo solo via
 * web,� anche possibile copiare questo file in "wp-config.php" e
 * rimepire i valori corretti.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - E? possibile ottenere questoe informazioni
// ** dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'dev');

/** Nome utente del database MySQL */
define('DB_USER', 'dev');

/** Password del database MySQL */
define('DB_PASSWORD', 'dev@@##');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha
idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * E' possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * E' possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ci� forzer� tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i/)w/EV`E^~j{8vRqSMQqCg!+`h;9NxO|!8[V?[}F>]?B}T+)gkyw`f*{t0YpN3=');
define('SECURE_AUTH_KEY',  ',|<D,xM# 3j*wVOMYG*my4.bCzpz4-jO]WrA_H5B|jl,qB{A|$,TzkN+HxY&,c-$');
define('LOGGED_IN_KEY',    'C-XXt9QraA#l$k3b8y`ighNPgN1#3+7h[^o+4D&+0P5k;0q:tOuGd_-9Lbj_GJ{.');
define('NONCE_KEY',        'IMq-GW|%|K!9n@~ftrds=<w-^8j-#3iw>^OQ($o|iZ+7wI>BUTdpQxCnB?$N$EhA');
define('AUTH_SALT',        'oqX=-2+Z/,<U$p->-o{+!3zu^kg=Jvc?5R2%GW~b|VIVQ{:G=V;ZPLl@)(@/Dv+Q');
define('SECURE_AUTH_SALT', '|m7hg@v@=|;}KCpy#zX:R^nIE.On{b]b>%|-?~B-~[AuMd,F`.Woa2/{MKkkQo~_');
define('LOGGED_IN_SALT',   '*2q;@+-6Q{58&<G9eR1!o.th# -?`91[~WYX eP:BE/ank1(,%-e%$6Z(}+-};%A');
define('NONCE_SALT',       '!Q+e9-#S *?u/*VDH=gP T@&I(sbp,YRMxIb *2IkDX1;=c`aT)|~JpyC.ZKU_CD');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress .
 *
 * E' possibile avere installazioni multiple su di un unico database if you give each a unique
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Lingua di Localizzazione di WordPress, di base Inglese.
 *
 * Modificare questa voce per localizzare WordPress. Occorre che nella cartella
 * wp-content/languages sia installato un file MO corrispondente alla lingua
 * selezionata. Ad esempio, installare de_DE.mo in to wp-content/languages ed
 * impostare WPLANG a 'de_DE' per abilitare il supporto alla lingua tedesca.
 *
 * Tale valore � gi� impostato per la lingua italiana
 */
define('WPLANG', 'it_IT');

/**
 * Per gli sviluppatori: modalit� di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * E' fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all'interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', true);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta lle variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
